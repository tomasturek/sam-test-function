
package phase
-------------
aws cloudformation [--profile {profile}] package --template-file app-sam.yaml --output-template-file app-sam-output.yaml --s3-bucket {s3-bucket}

deploy phase
------------
aws cloudformation [--profile {profile}] deploy --template-file app-sam-output.yaml --stack-name {STACK-NAME} --capabilities CAPABILITY_IAM
